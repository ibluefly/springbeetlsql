package com.park.oss.web.console;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.Park;
import com.park.oss.service.ParkService;
import com.park.oss.vo.ParkSum;

@Controller
@RequestMapping("/park")
public class ParkController {
	@Autowired
	ParkService service ;
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index() {
    	ModelAndView view = new ModelAndView("/park/index.btl");
    	PageQuery pageQuery = new PageQuery();
    	service.queryAll(pageQuery);
    	view.addObject("list",pageQuery.getList());
    	view.addObject("pageQuery",pageQuery);
    	
        return view;
    }
    
   
    @RequestMapping(value = "/add.html", method = RequestMethod.GET)
    public ModelAndView addPage() {
	    	ModelAndView view = new ModelAndView("/park/edit.btl");
	    return view;
    }
    
    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public ModelAndView editPage(Integer id) {
    		Park park = service.getPark(id);
	    	ModelAndView view = new ModelAndView("/park/edit.btl");
	    	view.addObject("park", park);
	    return view;
    }
    
    @RequestMapping(value = "/editAction.html", method = RequestMethod.POST)
    public ModelAndView editAction(Park park,Integer actionType) {
    		if(actionType==0){
    			park.setFreeAmount(0);
    			park.setUpdateTime(new Timestamp(new Date().getTime()));
    			service.addPark(park);
    		}else{
    			service.updatePark(park);
    		}
	    
	    	return index();
    }
    
    @RequestMapping(value = "/income.html", method = RequestMethod.GET)
    public ModelAndView sum(Integer parkId) {
    		List<ParkSum> list=  service.sum(parkId);
    	 	ModelAndView view = new ModelAndView("/park/income.btl");
	    	view.addObject("list", list);
	    	return view;
    }
}
