package com.park.oss.service;

import java.util.List;

import com.park.oss.model.Park;
import com.park.oss.model.SysUser;
import com.park.oss.vo.FreePark;


public interface ParkSearchService {
	 List<Park> search(Double lon,Double lat);
	 List<FreePark> searchFreePark(SysUser user,Double lon,Double lat,String day);
}
