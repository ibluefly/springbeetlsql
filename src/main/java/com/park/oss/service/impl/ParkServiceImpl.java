package com.park.oss.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.park.oss.model.Gate;
import com.park.oss.model.Park;
import com.park.oss.service.ParkService;
import com.park.oss.vo.ParkSum;
@Service
@Transactional
public class ParkServiceImpl implements ParkService {

	@Autowired
	SQLManager sqlManager ;
	@Override
	public void queryAll(PageQuery pageQuery) {
		
		sqlManager.pageQuery("park.search", Park.class, pageQuery);
	}
	@Override
	public void addPark(Park park) {
		sqlManager.insert(park);
		
	}
	@Override
	public void updatePark(Park park) {
		sqlManager.updateById(park);
		
	}
	@Override
	public void addGate(Gate gate) {
		sqlManager.insert(gate);
		
	}
	@Override
	public void removeGate(Integer id) {
		sqlManager.deleteById(Gate.class, id);
	}
	@Override
	public Park getPark(Integer id) {
		return sqlManager.unique(Park.class, id);
	}
	@Override
	public List<ParkSum> sum(Integer parkId) {
		Map paras = new HashMap();
		paras.put("parkId", parkId);
		return sqlManager.select("park.sum", ParkSum.class, null);
	}
	

}
