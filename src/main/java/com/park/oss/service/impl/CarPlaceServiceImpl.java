package com.park.oss.service.impl;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.park.oss.model.CarPlace;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.PlaceRentRule;
import com.park.oss.service.CarPlaceService;
import com.park.oss.vo.CarPlaceVo;

@Service
public class CarPlaceServiceImpl implements CarPlaceService {
	@Autowired
	SQLManager sqlManager ;
	@Override
	public List<CarPlaceVo> query(Integer parkId, String plate, String mobile) {
		
		CarPlace query = new CarPlace();
		query.setMobile(mobile);
		query.setPlate(plate);
		query.setParkId(parkId);
		return  sqlManager.select("carPlace.selectAll", CarPlaceVo.class, query,1,5);
	}
	@Override
	public void addPlaceRent(PlaceRent rent) {
		if(isExist(rent)) return ;
		sqlManager.insert(rent);
	}
	@Override
	public void addPlaceRentRule(PlaceRentRule rentRule) {
		sqlManager.insert(rentRule);
		
	}
	
	private boolean isExist(PlaceRent rent){
		int count = sqlManager.intValue("placeRent.exist", rent);
		return count!=0;
	}
	@Override
	public CarPlace getCarPlace(Long id) {
		return sqlManager.unique(CarPlace.class, id);
	}

}
