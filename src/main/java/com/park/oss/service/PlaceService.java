package com.park.oss.service;

import java.util.List;

import com.park.oss.model.CarPlace;
import com.park.oss.model.Place;
import com.park.oss.model.SysUser;
import com.park.oss.vo.PlaceVo;

public interface PlaceService {
	public List<PlaceVo> query(Integer pakId);
	
	public Integer order(CarPlace car);
	public void disableOrder(Integer id);
	public void enter(Integer id);
	public void pay(Integer id);
	public Double getPayMoney(SysUser user, Integer parkId);
	public void exit(Integer id);
	
	public List<PlaceVo> getUserPlace(Integer userId) ;
	public void addPlace(Place place);
}
