package com.park.oss.vo;

import java.util.Date;

public class ParkSum {
	Integer parkId;
	Double platformMoney;
	Double parkMoney;
	Double ownerMoney;
	Date minCreateTime;
	Date maxCreateTime;
	String parkName;
	public Integer getParkId() {
		return parkId;
	}
	public void setParkId(Integer parkId) {
		this.parkId = parkId;
	}
	public Double getPlatformMoney() {
		return platformMoney;
	}
	public void setPlatformMoney(Double platformMoney) {
		this.platformMoney = platformMoney;
	}
	public Double getParkMoney() {
		return parkMoney;
	}
	public void setParkMoney(Double parkMoney) {
		this.parkMoney = parkMoney;
	}
	public Double getOwnerMoney() {
		return ownerMoney;
	}
	public void setOwnerMoney(Double ownerMoney) {
		this.ownerMoney = ownerMoney;
	}
	public Date getMinCreateTime() {
		return minCreateTime;
	}
	public void setMinCreateTime(Date minCreateTime) {
		this.minCreateTime = minCreateTime;
	}
	public Date getMaxCreateTime() {
		return maxCreateTime;
	}
	public void setMaxCreateTime(Date maxCreateTime) {
		this.maxCreateTime = maxCreateTime;
	}
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	
	
}
