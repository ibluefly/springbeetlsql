package com.park.oss.model;
import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.TableTemplate;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-01-09
*/
@TableTemplate(" order by id desc ")
public class Park  {
	private Integer id ;
	private Integer amount ;
	private Integer freeAmount = 0;
	private Integer ownerRate ;
	private Integer parkRate ;
	//分层，按照百分比，比如10
	private Integer platformRate ;
	private Integer price ;
	//总共车位
	private Integer parkTotal ;
	private String address ;
	private Double lat ;
	private Double lon ;
	private String name ;
	private Timestamp updateTime ;
	
	//其他属性
	//距离
	private double distance;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getFreeAmount() {
		return freeAmount;
	}
	public void setFreeAmount(Integer freeAmount) {
		this.freeAmount = freeAmount;
	}
	public Integer getOwnerRate() {
		return ownerRate;
	}
	public void setOwnerRate(Integer ownerRate) {
		this.ownerRate = ownerRate;
	}
	public Integer getParkRate() {
		return parkRate;
	}
	public void setParkRate(Integer parkRate) {
		this.parkRate = parkRate;
	}
	public Integer getPlatformRate() {
		return platformRate;
	}
	public void setPlatformRate(Integer platformRate) {
		this.platformRate = platformRate;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	public Integer getParkTotal() {
		return parkTotal;
	}
	public void setParkTotal(Integer parkTotal) {
		this.parkTotal = parkTotal;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}

}
